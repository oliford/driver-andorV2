package andor2JNI;

import java.nio.ByteBuffer;

import otherSupport.SettingsManager;

/** Interface for Andor SDK v2 for non-CMOS cameras
 *  Calls through to AndorV2 but selects a given camera and try to make things thread safe 
 */
public class AndorV2SelectCam {
		
	private static String andorV2SyncObject = new String("andorV2SyncObject");
	
	
	static {
		System.load(SettingsManager.defaultGlobal().getPathProperty("andorV2.jniLibPath", "/usr/lib/andorJNI.so"));
	}
	
	private int lCameraHandle;
	
	public void Initialize(String dir){
		AndorV2.Initialize(dir);		
	}
	
	
	
	/** unsigned int SetReadMode(int mode); */
	public void SetReadMode(int mode){
		synchronized (andorV2SyncObject) {			
			AndorV2.SetCurrentCamera(lCameraHandle);
			AndorV2.SetReadMode(mode);
		}
	}
	
	/** unsigned int SetAcquisitionMode(int mode); */
	public  void SetAcquisitionMode(int mode){
		synchronized (andorV2SyncObject) {			
			AndorV2.SetCurrentCamera(lCameraHandle);		
			AndorV2.SetAcquisitionMode(mode);
		}
	}

	/** unsigned int SetShutter(int typ, int mode, int closingtime, int openingtime); */
	public  void SetShutter(int typ, int mode, int closingtime, int openingtime){
		synchronized (andorV2SyncObject) {			
			AndorV2.SetCurrentCamera(lCameraHandle);
			AndorV2.SetShutter(typ, mode, closingtime, openingtime);
		}
	}
	
	/** unsigned int SetExposureTime(float time); */
	public  void SetExposureTime(float time){
		synchronized (andorV2SyncObject) {			
			AndorV2.SetCurrentCamera(lCameraHandle);
			AndorV2.SetExposureTime(time);			
		}
	}
	
	/** unsigned int SetImage(int hbin, int vbin, int hstart, int hend, int vstart, int vend); */
	public  void SetImage(int hbin, int vbin, int hstart, int hend, int vstart, int vend){
		synchronized (andorV2SyncObject) {			
			AndorV2.SetCurrentCamera(lCameraHandle);
			AndorV2.SetImage(hbin, vbin, hstart, hend, vstart, vend);
		}
	}
	
	/** unsigned int GetDetector(int * xpixels, int * ypixels); */
	public  int[] GetDetector(){
		synchronized (andorV2SyncObject) {			
			AndorV2.SetCurrentCamera(lCameraHandle);
			return AndorV2.GetDetector();
		}
	}
	
	/** unsigned int StartAcquisition(); */
	public  void StartAcquisition();
	
	/** unsigned int WaitForAcquisition(); */
	public  void WaitForAcquisition();
	
	public  void WaitForAcquisitionTimeOut(int timeoutMS);
	
	/** unsigned int AbortAcquisition(); */
	public  void AbortAcquisition();
	
	/** unsigned int GetMostRecentImage(at_32 * arr, at_u32 size); */
	public  void GetMostRecentImage16(ByteBuffer imageData);
	
	/** unsigned int GetAcquisitionTimings(float * exposure, float * accumulate, float * kinetic); */
	public  float[] GetAcquisitionTimings();

	/** unsigned int ShutDown() */
	public  void ShutDown();
	
	/** unsigned int GetAvailableCameras(at_32 * totalCameras) */
	public  int GetAvailableCameras();

	/** unsigned int GetCameraHandle(at_32 cameraIndex, at_32 * cameraHandle); */
	public  int GetCameraHandle(int cameraIndex);

	/** unsigned int SetCurrentCamera(int lCameraHandle); */
	public  void SetCurrentCamera(int lCameraHandle);

	/** unsigned int GetTotalNumberImagesAcquired(at_32 * index); */
	public  int GetTotalNumberImagesAcquired();
	
	/** unsigned int WINAPI GetCameraSerialNumber (int* number) */
	public  int GetCameraSerialNumber();
	
	/** unsigned int WINAPI GetHeadModel(char* name) */
	public  String GetHeadModel();

	/** unsigned int WINAPI GetStatus(int* status) */
	public  int getStatus();
	
	/** unsigned int SetKineticCycleTime(float time); */
	public  void SetKineticCycleTime(float time);
	
	/** unsigned int SetAccumulationCycleTime(float time); */
	public  void SetAccumulationCycleTime(float time);
	
	/** unsigned int SetNumberAccumulations(int number); */
	public  void SetNumberAccumulations(int number);
	
	/** unsigned int SetNumberKinetics(int number); */
	public  void SetNumberKinetics(int number);

	/** This function will update the data array with the oldest image in the circular buffer. Once
		the oldest image has been retrieved it no longer is available. The data are returned as
		integers (16-bit unsigned integers). The "array" must be exactly the same size as the
		full image.*/
	public  void GetOldestImage16(ByteBuffer writableBuffer);

	
	/** Some EMCCD systems have the capability to use a second output amplifier. This
	function will set the type of output amplifier to be used when reading data from the head
	for these systems.
		0 – Standard EMCCD gain register (default)/Conventional(clara).
		1 – Conventional CCD register/Extended NIR mode(clara).
	*/
	public  void SetOutputAmplifier(int type);
	
	/**	This function will fill in an AndorCapabilities structure with the capabilities associated with
	the connected camera. */
	public AndorV2Capabilities GetCapabilities(){
		return new AndorV2Capabilities(_GetCapabilities());
	}
	
	private static  byte[] _GetCapabilities();

	/**As your Andor SDK system is capable of operating at more than one horizontal shift speed
		this function will return the actual number of speeds available.*/
	public  int GetNumberHSSpeeds(int channel, int typ);
	
	/**As your Andor system is capable of operating at more than one horizontal shift speed this
		function will return the actual speeds available. The value returned is in MHz. */
	public  float GetHSSpeed(int channel, int typ, int index);
	
	/** This function will set the speed at which the pixels are shifted into the output node during
		the readout phase of an acquisition. Typically your camera will be capable of operating at
		several horizontal shift speeds. To get the actual speed that an index corresponds to use
		the GetHSSpeed function. */
	public  void SetHSSpeed(int typ, int index);
	
	/**As your Andor system is capable of operating at more than one horizontal shift speed this
	function will return the actual speeds available. The values returned are in MHz. */
	public float[] getHSSpeeds(int channel, int typ){
		int n = GetNumberHSSpeeds(channel, typ);
		float ret[] = new float[n];
		for(int i=0; i < n; i++)
			ret[i] = GetHSSpeed(channel, typ, i);
		return ret;
	}
	
	/**As your Andor SDK system is capable of operating at more than one vertical shift speed
		this function will return the actual number of speeds available.*/
	public  int GetNumberVSSpeeds();
	
	/**As your Andor system is capable of operating at more than one vertical shift speed this
		function will return the actual speeds available. The value returned is in MHz. */
	public  float GetVSSpeed(int index);
	
	/** This function will set the vertical speed to be used for subsequent acquisitions */
	public  void SetVSSpeed(int index);
	
	/**As your Andor system is capable of operating at more than one vertical shift speed this
	function will return the actual speeds available. The value returned is in MHz. */
	public float[] getVSSpeeds(){
		int n = GetNumberVSSpeeds();
		float ret[] = new float[n];
		for(int i=0; i < n; i++)
			ret[i] = GetVSSpeed(i);
		return ret;
	}
	
	/** This function will normally return the number of vertical clock voltage amplitues that the
		camera has.*/
	public  int GetNumberVSAmplitudes();
	
	/** This Function is used to get the Vertical Clock Amplitude string that corresponds to the
		index passed in. */
	public  String GetVSAmplitudeString(int index);
	
	/** This Function is used to get the value of the Vertical Clock Amplitude found at the index
	passed in. */
	public  int GetVSAmplitudeValue(int index);
	
	public String[] getVSAmplitudes(){
		int n = GetNumberVSAmplitudes();
		String ret[] = new String[n];
		for(int i=0; i < n; i++)
			ret[i] = GetVSAmplitudeString(i);
		return ret;
	}
	/**If you choose a high readout speed (a low readout time), then you should also consider
		increasing the amplitude of the Vertical Clock Voltage.
		There are five levels of amplitude available for you to choose from:
		 Normal
		 +1
		 +2
		 +3
		 +4
		Exercise caution when increasing the amplitude of the vertical clock voltage, since higher
		clocking voltages may result in increased clock-induced charge (noise) in your signal. In
		general, only the very highest vertical clocking speeds are likely to benefit from an
		increased vertical clock voltage amplitude. */
	public  void SetVSAmplitude(int index);
	
	/** Available in some systems are a number of pre amp gains that can be applied to the
	data as it is read out. This function gets the number of these pre amp gains available.
	The functions GetPreAmpGain and SetPreAmpGain can be used to specify which of
	these gains is to be used. */
	public  int GetNumberPreAmpGains();
	
	/** For those systems that provide a number of pre amp gains to apply to the data as it is read
	out; this function retrieves the amount of gain that is stored for a particular index. The
	number of gains available can be obtained by calling the GetNumberPreAmpGains
	function and a specific Gain can be selected using the function SetPreAmpGain. */
	public  float GetPreAmpGain(int index); 
	
	/** This function will set the pre amp gain to be used for subsequent acquisitions. The actual
	gain factor that will be applied can be found through a call to the GetPreAmpGain
	function. */
	public  void SetPreAmpGain(int index);
	
	/** For those systems that provide a number of pre amp gains to apply to the data as it is read
	out; this function retrieves the amount of gain that is stored for a particular index. */
	public float[] getPreAmpGains(){
		int n = GetNumberPreAmpGains();
		float ret[] = new float[n];
		for(int i=0; i < n; i++){
			try{
				ret[i] = GetPreAmpGain(i);
			}catch(AndorV2Exception err){
				if(err.getErrorCode() == AndorV2Exception.DRV_P1INVALID){
					System.err.println("Unable to read PreAmp gain " + i + ": " + err.getMessage());
					//this makes no sense, but happens sometimes
					ret[i] = -1;
					
				}else
					throw err;
			}
		}
		return ret;
	}

	/** Returns the current gain setting. The meaning of the value returned depends on the EM
		Gain mode.*/
	public  int GetEMCCDGain();
	
	/** Returns the minimum and maximum values of the current selected EM Gain mode
		" ... and temperature of the sensor." <-- I think this bit is nonsense */
	public  int[] GetEMGainRange();	
	
	/** Allows the user to change the gain value. The valid range for the gain depends on what
		gain mode the camera is operating in. See SetEMGainMode to set the mode and
		GetEMGainRange to get the valid range to work with. To access higher gain values
		(>x300) see SetEMAdvanced. */
	public  void SetEMCCDGain(int gain);
	
	/** Set the EM Gain mode to one of the following possible settings.
		Mode 0: The EM Gain is controlled by DAC settings in the range 0-255. Default mode.
		1: The EM Gain is controlled by DAC settings in the range 0-4095.
		2: Linear mode.
		3: Real EM gain
		To access higher gain values (if available) it is necessary to enable advanced EM gain,
		see SetEMAdvanced. */
	public  void SetEMGainMode(int mode);
	/** This is a function that allows the setting up of random tracks with more options that the
			SetRandomTracks function.
			The minimum number of tracks is 1. The maximum number of tracks is the number of
			vertical pixels.
			There is a further limit to the number of tracks that can be set due to memory constraints
			in the camera. It is not a fixed number but depends upon the combinations of the tracks.
			For example, 20 tracks of different heights will take up more memory than 20 tracks of
			the same height.
			If attempting to set a series of random tracks and the return code equals
			DRV_RANDOM_TRACK_ERROR, change the makeup of the tracks to have more
			repeating heights and gaps so less memory is needed.
			Each track must be defined by a group of six integers.
			-The top and bottom positions of the tracks.
			-The left and right positions for the area of interest within each track
			-The horizontal and vertical binning for each track.
			The positions of the tracks are validated to ensure that the tracks are in increasing order.
			The left and right positions for each track must be the same.
			For iXon the range is between 8 and CCD width, inclusive
			For idus the range must be between 257 and CCD width, inclusive.
			Horizontal binning must be an integer between 1 and 64 inclusive, for iXon.
			Horizontal binning is not implementated for iDus and must be set to 1.
			Vertical binning is used in the following way. A track of:
			1 10 1 1024 1 2
			is actually implemented as 5 tracks of height 2. . Note that a vertical binning of 1 will have
			the effect of vertically binning the entire track; otherwise vertical binning will operate as
			normal.
			1 2 1 1024 1 1
			3 4 1 1024 1 1
			5 6 1 1024 1 1
			7 8 1 1024 1 1
			9 10 1 1024 1 1*/
	public  void SetComplexImage(int[] areas);
	
	/** This function will set the desired temperature of the detector. To turn the cooling ON and
			OFF use the CoolerON and CoolerOFF function respectively. */
	public  void SetTemperature(int temperature);
	
	/** Switches ON the cooling. On some systems the rate of temperature change is controlled
		until the temperature is within 3o of the set value. Control is returned immediately to the
		calling application. */
	public  void CoolerON();
	
	/** Switches OFF the cooling. The rate of temperature change is controlled in some models
		until the temperature reaches 0o. Control is returned immediately to the calling
		application. */
	public  void CoolerOFF();
	
	/** This function returns the valid range of temperatures in centigrade to which the detector
		can be cooled. */
	public  int[] GetTemperatureRange();
	
	
	/** This function returns the temperature in degrees of the detector ret[0] 
	 *     It also gives the status of cooling process as ((int)ret[1])
			
			DRV_TEMP_OFF Temperature is OFF.
			DRV_TEMP_STABILIZED Temperature has stabilized at set point.
			DRV_TEMP_NOT_REACHED Temperature has not reached set point.
			DRV_TEMP_DRIFT Temperature had stabilised but has since drifted
			DRV_TEMP_NOT_STABILIZED Temperature reached but not stabilized
	  */
	public  float[] GetTemperatureF();
	
	
	/** Description This function will set the trigger mode that the camera will operate in.	
		0. Internal
		1. External
		6. External Start
		7. External Exposure (Bulb)
		9. External FVB EM (only valid for EM Newton models in FVB mode)
		10. Software Trigger
		12. External Charge Shifting */
	public  void SetTriggerMode(int mode);
	
	
	/** This function will enable fast external triggering. When fast external triggering is enabled
	the system will NOT wait until a “Keep Clean” cycle has been completed before
	accepting the next trigger. This setting will only have an effect if the trigger mode has
	been set to External via SetTriggerMode. */
	public  void SetFastExtTrigger(int mode);
		 
	/** This function will return the time of the initial frame and the time in milliseconds of further
		frames from this point.
		@returns			
		  0: wYear - start time
		  1: wMonth - start time
		  2: wDayOfWeek - start time
		  3: wDay - start time
		  4: wHour - start time
		  5: wMinute - start time
		  6: wSecond - start time
		  7: wMilliseconds - start time
		 */
	public  short[] GetMetaDataInfoStartTime();

	/** This function will return the time in milliseconds of frames from GetMetaDataInfoStartTime() */		
	public  float GetMetaDataInfoRelativeFrameTime(int index);

	/** This function activates the meta data option. */
	public  void SetMetaData(boolean enable);
	
	
	/** This function will set whether an acquisition will readout in Frame Transfer Mode. If the
		acquisition mode is Single Scan or Fast Kinetics this call will have no affect. */
	public  void SetFrameTransferMode(boolean enable);
	
	/** This function will return the maximum Exposure Time in seconds that is settable by the */
	public  float GetMaximumExposure();
	
	/** This function will return the maximum number of images the circular buffer can store
		based on the current acquisition settings.*/
	public  int GetSizeOfCircularBuffer();

	/** This function will return information on the number of available images in the circular
		buffer. This information can be used with GetImages to retrieve a series of images. If any
		images are overwritten in the circular buffer they no longer can be retrieved and the
		information returned will treat overwritten images as not available. */
	public  int[] GetNumberAvailableImages();
	

	/** This function will return information on the number of new images (i.e. images which have
		not yet been retrieved) in the circular buffer. This information can be used with
		GetImages to retrieve a series of the latest images. If any images are overwritten in the
		circular buffer they can no longer be retrieved and the information returned will treat
		overwritten images as having been retrieved. */
	public  int[] GetNumberNewImages();
	
	/** 16-bit version of the GetImages function.
	 * This function will update the data array with the specified series of images from the
		circular buffer. If the specified series is out of range (i.e. the images have been
		overwritten or have not yet been acquired then an error will be returned.
		@return int[]{ validFirst, validLast } */
	public  int[] GetImages16(int first, int last, ByteBuffer writableBuffer);
	
}
