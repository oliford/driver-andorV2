import andor2JNI.AndorV2;
import andor2JNI.AndorV2Exception;
import otherSupport.SettingsManager;

public class TestCameraSelection {

	public static void main(String[] args) {
		new SettingsManager("minerva", true);
		
		//System.out.println("Initialize...");
		//AndorV2.Initialize(SettingsManager.defaultGlobal().getPathProperty("andorV2.andorConfigPath", "/usr/local/etc/andor"));
		
		System.out.print("GetAvailableCameras...");
		int n = AndorV2.GetAvailableCameras();
		
		System.out.println("nCameras = " + n);
		
		for(int i=0; i < n; i++) {
			try {
				//if(i!=0)
				//	continue;
				int cameraHandle = AndorV2.GetCameraHandle(i);
				System.out.println(i+": handel = " + cameraHandle);
				AndorV2.SetCurrentCamera(cameraHandle);

				System.out.println("Initialize...");
				AndorV2.Initialize(SettingsManager.defaultGlobal().getPathProperty("andorV2.andorConfigPath", "/usr/local/etc/andor"));
				
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) { }
				int serial = AndorV2.GetCameraSerialNumber();
				String model = AndorV2.GetHeadModel();
					
				System.out.println(i+": model=" + model + ", S/N=" + serial);  
				//System.out.println("Camera "+i+":handel = " + cameraHandle);// , Head model=" + model + ", S/N=" + serial);
				
				
			
			}catch(AndorV2Exception err) {
				System.err.println("Error getting data for cameras index " + i + ": " + err.getMessage());
			}
		}
		
		AndorV2.ShutDown();
	}
	
}
