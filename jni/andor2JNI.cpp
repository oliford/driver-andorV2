// JNI gateway to Andor V2 SDK



#include "andor2JNI_AndorV2.h"

#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <atmcdLXd.h>
#include <locale.h>

/* Check return code from Andor V2 SDK functions and throw exception if necessary */
void checkReturn(const char *funcName, JNIEnv *env, int returnCode){
	if(returnCode == DRV_SUCCESS)
		return; //all OK


	// Find and instantiate the exception
	jclass exClass = env->FindClass("andor2JNI/AndorV2Exception");

	jmethodID constructor = env->GetMethodID( exClass, "<init>", "(Ljava/lang/String;I)V");
	if(constructor == NULL){
		fprintf(stderr, "*** JNI: CANNOT FIND EXCEPTION CONSTRUCTOR ***");
		return;
	}
	jstring jStr = env->NewStringUTF(funcName);
	jobject exception = env->NewObject(exClass, constructor, jStr, returnCode);

	// Throw the exception. Since this is native code,
	// execution continues, and the execution will be abruptly
	// interrupted at the point in time when we return to the VM. 
	// The calling code will perform the early return back to Java code.
	env->Throw((jthrowable) exception);

	// Clean up local reference
	env->DeleteLocalRef(exClass);
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    Initialize
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2__1Initialize
  (JNIEnv *env, jclass cls, jstring jStrDir){

	//some cameras don't work properly if the locale is set to something weird
	//we force it here, but warn the caller since we've actually changed the locale
	//of the whole JVM
        char *lc = setlocale(LC_ALL, NULL);

	if(strcmp(lc,"C")){
		fprintf(stderr,"WARNING: Changing JVM locale to 'C' to avoid bugs in Andor2 SDK");
		fflush(stderr);
		lc = setlocale(LC_ALL, "C");
	}

	char *cstrDir = (char *)env->GetStringUTFChars(jStrDir, NULL);

	unsigned int ret = Initialize(cstrDir);
	checkReturn("Initialize", env, ret);

	env->ReleaseStringUTFChars(jStrDir, cstrDir);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetReadMode
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetReadMode
  (JNIEnv *env, jclass cls, jint mode){

	unsigned int ret = SetReadMode(mode);
	checkReturn("SetReadMode", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetAcquisitionMode
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetAcquisitionMode
  (JNIEnv *env, jclass cls, jint mode){

	unsigned int ret = SetAcquisitionMode(mode);
	checkReturn("SetAcquisitionMode", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetShutter
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetShutter
  (JNIEnv *env, jclass cls, jint typ, jint mode, jint closingtime, jint openingtime){

	unsigned int ret = SetShutter(typ, mode, closingtime, openingtime);
	checkReturn("??", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetExposureTime
 * Signature: (F)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetExposureTime
  (JNIEnv *env, jclass cls, jfloat time){

	unsigned int ret = SetExposureTime(time);
	checkReturn("SetExposureTime", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetImage
 * Signature: (IIIIII)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetImage
  (JNIEnv *env, jclass cls, jint hbin, jint vbin, jint hstart, jint hend, jint vstart, jint vend){

	unsigned int ret = SetImage(hbin, vbin, hstart, hend, vstart, vend);
	checkReturn("SetImage", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetDetector
 * Signature: ()[I
 */
JNIEXPORT jintArray JNICALL Java_andor2JNI_AndorV2_GetDetector
  (JNIEnv *env, jclass cls){

	int xpixels = -1, ypixels = -1;

	unsigned int ret = GetDetector(&xpixels, &ypixels);
	checkReturn("GetDetector", env, ret);

	jintArray retArr = env->NewIntArray(2);
	if (retArr == NULL){
		fprintf(stderr, "Andor2JNI: GetDetector(): Couldn't allocate int array in JNI land.");
		fflush(stderr);
		checkReturn("??", env, -73); //throw AndorV2Exception("JNI alloction failed");
		return NULL;
	}	

	env->SetIntArrayRegion(retArr, 0, 1, (int32_t*)&xpixels);
	env->SetIntArrayRegion(retArr, 1, 1, (int32_t*)&ypixels);

	return retArr;
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    StartAcquisition
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_StartAcquisition
  (JNIEnv *env, jclass cls){

	unsigned int ret = StartAcquisition();
	checkReturn("??", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    WaitForAcquisition
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_WaitForAcquisition
  (JNIEnv *env, jclass cls){

	unsigned int ret = WaitForAcquisition();
	checkReturn("WaitForAcquisition", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    WaitForAcquisitionTimeOut
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_WaitForAcquisitionTimeOut
  (JNIEnv *env, jclass cls, jint timeoutMS){

	unsigned int ret = WaitForAcquisitionTimeOut(timeoutMS);
	checkReturn("WaitForAcquisitionTimeOut", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    AbortAcquisition
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_AbortAcquisition
  (JNIEnv *env, jclass cls){

	unsigned int ret = AbortAcquisition();
	checkReturn("AbortAcquisition", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetAcquisitionTimings
 * Signature: ()[F
 */
JNIEXPORT jfloatArray JNICALL Java_andor2JNI_AndorV2_GetAcquisitionTimings
  (JNIEnv *env, jclass cls){

	float exposure = 0, accumulate = 0, kinetic = 0;

	unsigned int ret = GetAcquisitionTimings(&exposure, &accumulate, &kinetic);
	checkReturn("GetAcquisitionTimings", env, ret);
	//printf("acq timings: exp=%f, acc=%f, kin=%fn", exposure, accumulate, kinetic);

	jfloatArray retArr = env->NewFloatArray(3);
	if (retArr == NULL){
		fprintf(stderr, "Andor2JNI: GetAcquisitionTimings(): Couldn't allocate float array in JNI land.");
		fflush(stderr);
		checkReturn("??", env, -73); //throw AndorV2Exception("JNI alloction failed");
		return NULL;
	}	

	env->SetFloatArrayRegion(retArr, 0, 1, &exposure);
	env->SetFloatArrayRegion(retArr, 1, 1, &accumulate);
	env->SetFloatArrayRegion(retArr, 2, 1, &kinetic);

	return retArr;
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    ShutDown
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_ShutDown
  (JNIEnv *env, jclass cls){

	unsigned int ret = ShutDown();
	checkReturn("ShutDown", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetAvailableCameras
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_andor2JNI_AndorV2_GetAvailableCameras
  (JNIEnv *env, jclass cls){

	at_32 totalCameras = -1;

	unsigned int ret = GetAvailableCameras(&totalCameras);
	checkReturn("GetAvailableCameras", env, ret);

	return totalCameras;
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetCameraHandle
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_andor2JNI_AndorV2_GetCameraHandle
  (JNIEnv *env, jclass cls, jint cameraIndex){
	at_32 cameraHandle = 0;

	unsigned int ret = GetCameraHandle(cameraIndex, &cameraHandle);
	checkReturn("GetCameraHandle", env, ret);

	return cameraHandle;
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetCurrentCamera
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetCurrentCamera
  (JNIEnv *env, jclass cls, jint cameraHandle){

	unsigned int ret = SetCurrentCamera(cameraHandle);
	checkReturn("SetCurrentCamera", env, ret);
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetTotalNumberImagesAcquired
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_andor2JNI_AndorV2_GetTotalNumberImagesAcquired
  (JNIEnv *env, jclass cls){
	
	at_32 index = -1;
	unsigned int ret = GetTotalNumberImagesAcquired(&index);

	checkReturn("GetTotalNumberImagesAcquired", env, ret);

	return index;
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetMostRecentImage16
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_GetMostRecentImage16
  (JNIEnv *env, jclass cls, jobject jByteBuffer){

	jbyte *cBufferPtr = (jbyte *)env->GetDirectBufferAddress(jByteBuffer);
	if(cBufferPtr == NULL){ 
		fprintf(stderr, "v: GetMostRecentImage16(): Couldn't get ByteBuffer memory in JNI land. Is it a direct buffer?");
		fflush(stderr);
		checkReturn("??", env, -73); //throw PICamSDKException("JNI alloction failed");
		return;
	}
	long bufferLen = env->GetDirectBufferCapacity(jByteBuffer);

	unsigned int ret = GetMostRecentImage16((unsigned short *)cBufferPtr, bufferLen/2);
	checkReturn("GetMostRecentImage16", env, ret);
}



/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetCameraSerialNumber
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_andor2JNI_AndorV2_GetCameraSerialNumber
  (JNIEnv *env, jclass cls){

	int serialNumber = -1;

	unsigned int ret = GetCameraSerialNumber(&serialNumber);
	checkReturn("GetCameraSerialNumber", env, ret);

	return serialNumber;
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetHeadModel
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_andor2JNI_AndorV2_GetHeadModel
  (JNIEnv *env, jclass cls){

	//no idea how big this should be, go big and hope for the best...
	char cStr[1024]; 

	unsigned int ret = GetHeadModel(cStr);

	jstring jStr = env->NewStringUTF(cStr); 

	checkReturn("GetHeadModel", env,ret);	

	return jStr;
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    getStatus
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_andor2JNI_AndorV2_getStatus
  (JNIEnv *env, jclass cls){

	int status = 0;

	unsigned int ret = GetStatus(&status);
	checkReturn("GetStatus", env, ret);

	return status;	
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetKineticCycleTime
 * Signature: (F)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetKineticCycleTime
  (JNIEnv *env, jclass cls, jfloat time){

	unsigned int ret = SetKineticCycleTime(time);
	checkReturn("SetKineticCycleTime", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetAccumulationCycleTime
 * Signature: (F)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetAccumulationCycleTime
  (JNIEnv *env, jclass cls, jfloat time){

	unsigned int ret = SetAccumulationCycleTime(time);
	checkReturn("??", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetNumberAccumulations
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetNumberAccumulations
  (JNIEnv *env, jclass cls, jint number){

	unsigned int ret = SetNumberAccumulations(number);
	checkReturn("SetNumberAccumulations", env, ret);
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetNumberKinetics
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetNumberKinetics
  (JNIEnv *env, jclass cls, jint number){

	unsigned int ret = SetNumberKinetics(number);
	checkReturn("SetNumberKinetics", env, ret);
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetOldestImage16
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_GetOldestImage16
	(JNIEnv *env, jclass cls, jobject jByteBuffer){

	jbyte *cBufferPtr = (jbyte *)env->GetDirectBufferAddress(jByteBuffer);
	if(cBufferPtr == NULL){ 
		fprintf(stderr, "v: GetOldestImage16(): Couldn't get ByteBuffer memory in JNI land. Is it a direct buffer?");
		fflush(stderr);
		checkReturn("??", env, -73); //throw PICamSDKException("JNI alloction failed");
		return;
	}
	long bufferLen = env->GetDirectBufferCapacity(jByteBuffer);

	unsigned int ret = GetOldestImage16((unsigned short *)cBufferPtr, bufferLen/2);
	checkReturn("GetOldestImage16", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetOutputAmplifier
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetOutputAmplifier
	(JNIEnv *env, jclass cls, jint type){

	unsigned int ret = SetOutputAmplifier(type);
	checkReturn("SetOutputAmplifier", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    _GetCapabilities
 * Signature: ()[B
 */
JNIEXPORT jbyteArray JNICALL Java_andor2JNI_AndorV2__1GetCapabilities
  (JNIEnv *env, jclass cls){
	
	//byte array to return ID structure
	jbyteArray retArr = env->NewByteArray(sizeof(AndorCapabilities));
	if (retArr == NULL){
		fprintf(stderr, "PICam: getCameraID(): Couldn't allocate byte array in JNI land.");
		fflush(stderr);
		checkReturn("??", env, -73); //throw PICamSDKException("JNI alloction failed");
		return NULL;
	}
	jbyte *buffer = env->GetByteArrayElements(retArr, 0);

	memset(buffer, 0, sizeof(AndorCapabilities));

	((AndorCapabilities*)buffer)->ulSize = sizeof(AndorCapabilities);

	unsigned int ret = GetCapabilities((AndorCapabilities*)buffer);

	env->ReleaseByteArrayElements(retArr, buffer, 0);

	checkReturn("GetCapabilities", env, ret);

	return retArr;
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetNumberHSSpeeds
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_andor2JNI_AndorV2_GetNumberHSSpeeds
  (JNIEnv *env, jclass cls, jint channel, jint typ){

	int number = -1;

	unsigned int ret = GetNumberHSSpeeds(channel, typ, &number);
	checkReturn("GetNumberHSSpeeds", env, ret);

	return number;
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetHSSpeed
 * Signature: (III)F
 */
JNIEXPORT jfloat JNICALL Java_andor2JNI_AndorV2_GetHSSpeed
  (JNIEnv *env, jclass cls, jint channel, jint typ, jint index){

	float value = 0;

	unsigned int ret = GetHSSpeed(channel, typ, index, &value);
	checkReturn("GetHSSpeed", env, ret);

	return value;	
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetHSSpeed
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetHSSpeed
  (JNIEnv *env, jclass cls, jint typ, jint index){

	unsigned int ret = SetHSSpeed(typ, index);
	checkReturn("??", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetNumberVSSpeeds
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_andor2JNI_AndorV2_GetNumberVSSpeeds
  (JNIEnv *env, jclass cls){

	int number = -1;

	unsigned int ret = GetNumberVSSpeeds(&number);
	checkReturn("GetNumberVSSpeeds", env, ret);

	return number;
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetVSSpeed
 * Signature: (I)F
 */
JNIEXPORT jfloat JNICALL Java_andor2JNI_AndorV2_GetVSSpeed
  (JNIEnv *env, jclass cls, jint index){

	float value = 0;

	unsigned int ret = GetVSSpeed(index, &value);
	checkReturn("GetVSSpeed", env, ret);

	return value;	
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetVSSpeed
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetVSSpeed
  (JNIEnv *env, jclass cls, jint index){

	unsigned int ret = SetVSSpeed(index);
	checkReturn("SetVSSpeed", env, ret);
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetNumberVSAmplitudes
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_andor2JNI_AndorV2_GetNumberVSAmplitudes
  (JNIEnv *env, jclass cls){

	int number = -1;

	unsigned int ret = GetNumberVSAmplitudes(&number);
	checkReturn("GetNumberVSAmplitudes", env, ret);

	return number;
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetVSAmplitudeString
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_andor2JNI_AndorV2_GetVSAmplitudeString
  (JNIEnv *env, jclass cls, jint index){

	char str[256]; //how big??? dangerous!!

	unsigned int ret = GetVSAmplitudeString(index, str);
	checkReturn("GetVSAmplitudeString", env, ret);

	jstring jStr = env->NewStringUTF(str);

	return jStr;	
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetVSAmplitudeValue
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_andor2JNI_AndorV2_GetVSAmplitudeValue
  (JNIEnv *env, jclass cls, jint index){

	int value = 0;

	unsigned int ret = GetVSAmplitudeValue(index, &value);
	checkReturn("GetVSAmplitudeValue", env, ret);

	return value;	
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetVSAmplitude
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetVSAmplitude
  (JNIEnv *env, jclass cls, jint index){

	unsigned int ret = SetVSAmplitude(index);
	checkReturn("SetVSAmplitude", env, ret);
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetNumberPreAmpGains
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_andor2JNI_AndorV2_GetNumberPreAmpGains
  (JNIEnv *env, jclass cls){

	int number = -1;

	unsigned int ret = GetNumberPreAmpGains(&number);
	checkReturn("GetNumberPreAmpGains", env, ret);

	return number;
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetPreAmpGain
 * Signature: (I)F
 */
JNIEXPORT jfloat JNICALL Java_andor2JNI_AndorV2_GetPreAmpGain
  (JNIEnv *env, jclass cls, jint index){

	float value = 0;

	unsigned int ret = GetPreAmpGain(index, &value);
	checkReturn("GetPreAmpGain", env, ret);

	return value;	
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetPreAmpGain
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetPreAmpGain
  (JNIEnv *env, jclass cls, jint index){

	unsigned int ret = SetPreAmpGain(index);
	checkReturn("SetPreAmpGain", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetEMCCDGain
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_andor2JNI_AndorV2_GetEMCCDGain
  (JNIEnv *env, jclass cls){

	int value = 0;

	unsigned int ret = GetEMCCDGain(&value);
	checkReturn("GetEMCCDGain", env, ret);

	return value;	
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetEMGainRange
 * Signature: ()[I
 */
JNIEXPORT jintArray JNICALL Java_andor2JNI_AndorV2_GetEMGainRange
  (JNIEnv *env, jclass cls){

	int low = 0;
	int high = 0;

	unsigned int ret = GetEMGainRange(&low, &high);
	checkReturn("GetEMGainRange", env, ret);

	jintArray retArr = env->NewIntArray(2);
	if (retArr == NULL){
		fprintf(stderr, "Andor2JNI: GetEMGainRange(): Couldn't allocate int array in JNI land.");
		fflush(stderr);
		checkReturn("GetEMGainRange", env, -73); //throw AndorV2Exception("JNI alloction failed");
		return NULL;
	}	

	env->SetIntArrayRegion(retArr, 0, 1, &low);
	env->SetIntArrayRegion(retArr, 1, 1, &high);

	return retArr;	
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetEMCCDGain
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetEMCCDGain
  (JNIEnv *env, jclass cls, jint index){

	unsigned int ret = SetEMCCDGain(index);
	checkReturn("SetEMCCDGain", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetEMGainMode
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetEMGainMode
  (JNIEnv *env, jclass cls, jint mode){

	unsigned int ret = SetEMGainMode(mode);
	checkReturn("SetEMGainMode", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetComplexImage
 * Signature: ([I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetComplexImage
  (JNIEnv *env, jclass cls, jintArray areas){

	jint *cArr = env->GetIntArrayElements(areas, 0);
	int nAreas = env->GetArrayLength(areas) / 6;

	unsigned int ret = SetComplexImage(nAreas, cArr);
	
	env->ReleaseIntArrayElements(areas, cArr, 0);

	checkReturn("SetComplexImage", env, ret);
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetTemperature
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetTemperature
  (JNIEnv *env, jclass cls, jint temp){

	unsigned int ret = SetTemperature(temp);
	checkReturn("SetTemperature", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    CoolerON
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_CoolerON
  (JNIEnv *env, jclass cls){

	unsigned int ret = CoolerON();
	checkReturn("CoolerON", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    CoolerOFF
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_CoolerOFF
  (JNIEnv *env, jclass cls){

	unsigned int ret = CoolerOFF();
	checkReturn("CoolerOFF", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetTemperatureRange
 * Signature: ()[I
 */
JNIEXPORT jintArray JNICALL Java_andor2JNI_AndorV2_GetTemperatureRange
  (JNIEnv *env, jclass cls){

	int min = -1, max = -1;

	unsigned int ret = GetTemperatureRange(&min, &max);
	checkReturn("??", env, ret);

	jintArray retArr = env->NewIntArray(2);
	if (retArr == NULL){
		fprintf(stderr, "Andor2JNI: GetTemperatureRange(): Couldn't allocate int array in JNI land.");
		fflush(stderr);
		checkReturn("??", env, -73); //throw AndorV2Exception("JNI alloction failed");
		return NULL;
	}	

	env->SetIntArrayRegion(retArr, 0, 1, (int32_t*)&min);
	env->SetIntArrayRegion(retArr, 1, 1, (int32_t*)&max);

	return retArr;
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetTemperatureF
 * Signature: ()[F
 */
JNIEXPORT jfloatArray JNICALL Java_andor2JNI_AndorV2_GetTemperatureF
  (JNIEnv *env, jclass cls){

	float value = 0;

	unsigned int ret = GetTemperatureF(&value);

	//don't throw errors on 'temp' return code, we use that as the status
	if(ret < DRV_TEMP_CODES || ret > DRV_TEMP_DRIFT)
		checkReturn("GetTemperatureF", env, ret);

	jfloatArray retArr = env->NewFloatArray(2);
	if (retArr == NULL){
		fprintf(stderr, "Andor2JNI: GetTemperatureF(): Couldn't allocate int array in JNI land.");
		fflush(stderr);
		checkReturn("GetTemperatureF", env, -73); //throw AndorV2Exception("JNI alloction failed");
		return NULL;
	}	

	float retAsFloat = ret;

	env->SetFloatArrayRegion(retArr, 0, 1, &value);
	env->SetFloatArrayRegion(retArr, 1, 1, &retAsFloat);

	return retArr;
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetTriggerMode
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetTriggerMode
  (JNIEnv *env, jclass cls, jint mode){

	unsigned int ret = SetTriggerMode(mode);
	checkReturn("SetTriggerMode", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetFastExtTrigger
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetFastExtTrigger
  (JNIEnv *env, jclass cls, jint enable){

	unsigned int ret = SetFastExtTrigger(enable);
	checkReturn("SetFastExtTrigger", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetMetaDataInfoStartTime
 * Signature: ()[S
 */
JNIEXPORT jshortArray JNICALL Java_andor2JNI_AndorV2_GetMetaDataInfoStartTime
  (JNIEnv *env, jclass cls){

	SYSTEMTIME TimeOfStart;
	float pfTimeFromStart = 0;

	unsigned int ret = GetMetaDataInfo(&TimeOfStart, &pfTimeFromStart, 0);
	checkReturn("GetMetaDataInfo", env, ret);


	jshortArray retArr = env->NewShortArray(8);
	if (retArr == NULL){
		fprintf(stderr, "Andor2JNI: GetMetaDataInfoStartTime(): Couldn't allocate int array in JNI land.");
		fflush(stderr);
		checkReturn("??", env, -73); //throw AndorV2Exception("JNI alloction failed");
		return NULL;
	}	

	env->SetShortArrayRegion(retArr, 0, 8, (int16_t*)&TimeOfStart);

	return retArr;	
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetMetaDataInfoRelativeFrameTime
 * Signature: (I)F
 */
JNIEXPORT jfloat JNICALL Java_andor2JNI_AndorV2_GetMetaDataInfoRelativeFrameTime
  (JNIEnv *env, jclass cls, jint index){

	SYSTEMTIME TimeOfStart;
	float pfTimeFromStart = 0;

	unsigned int ret = GetMetaDataInfo(&TimeOfStart, &pfTimeFromStart, index);
	checkReturn("GetMetaDataInfo", env, ret);

	return pfTimeFromStart;	
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetMetaData
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetMetaData
  (JNIEnv *env, jclass cls, jboolean enable){

	unsigned int ret = SetMetaData(enable);
	checkReturn("SetMetaData", env, ret);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    SetFrameTransferMode
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_SetFrameTransferMode
  (JNIEnv *env, jclass cls, jboolean enable){

	unsigned int ret = SetFrameTransferMode(enable);
	checkReturn("SetFrameTransferMode", env, ret);
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetMaximumExposure
 * Signature: ()F
 */
JNIEXPORT jfloat JNICALL Java_andor2JNI_AndorV2_GetMaximumExposure
  (JNIEnv *env, jclass cls){

	float value = 0;

	unsigned int ret = GetMaximumExposure(&value);
	checkReturn("GetMaximumExposure", env, ret);

	return value;
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetSizeOfCircularBuffer
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_andor2JNI_AndorV2_GetSizeOfCircularBuffer
  (JNIEnv *env, jclass cls){

	int value = 0;

	unsigned int ret = GetSizeOfCircularBuffer(&value);
	checkReturn("GetSizeOfCircularBuffer", env, ret);

	return value;
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetNumberAvailableImages
 * Signature: ()[I
 */
JNIEXPORT jintArray JNICALL Java_andor2JNI_AndorV2_GetNumberAvailableImages
  (JNIEnv *env, jclass cls){

	int first=0, last=0;

	unsigned int ret = GetNumberAvailableImages(&first, &last);
	checkReturn("GetNumberAvailableImages", env, ret);
	
	jintArray retArr = env->NewIntArray(2);
	if (retArr == NULL){
		fprintf(stderr, "Andor2JNI: Java_andor2JNI_AndorV2_GetNumberAvailableImages(): Couldn't allocate int array in JNI land.");
		fflush(stderr);
		checkReturn("Java_andor2JNI_AndorV2_GetNumberAvailableImages", env, -73); //throw AndorV2Exception("JNI alloction failed");
		return NULL;
	}	

	env->SetIntArrayRegion(retArr, 0, 1, (int32_t*)&first);
	env->SetIntArrayRegion(retArr, 1, 1, (int32_t*)&last);

	return retArr;
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetNumberNewImages
 * Signature: ()[I
 */
JNIEXPORT jintArray JNICALL Java_andor2JNI_AndorV2_GetNumberNewImages
  (JNIEnv *env, jclass cls){

	int first=0, last=0;

	unsigned int ret = GetNumberNewImages(&first, &last);
	checkReturn("GetNumberNewImages", env, ret);
	
	jintArray retArr = env->NewIntArray(2);
	if (retArr == NULL){
		fprintf(stderr, "Andor2JNI: GetNumberNewImages(): Couldn't allocate int array in JNI land.");
		fflush(stderr);
		checkReturn("GetNumberNewImages", env, -73); //throw AndorV2Exception("JNI alloction failed");
		return NULL;
	}	

	env->SetIntArrayRegion(retArr, 0, 1, (int32_t*)&first);
	env->SetIntArrayRegion(retArr, 1, 1, (int32_t*)&last);

	return retArr;
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    GetImages16
 * Signature: (IILjava/nio/ByteBuffer;)[I
 */
JNIEXPORT jintArray JNICALL Java_andor2JNI_AndorV2_GetImages16
  (JNIEnv *env, jclass cls, jint first, jint last, jobject jByteBuffer){

	jbyte *cBufferPtr = (jbyte *)env->GetDirectBufferAddress(jByteBuffer);
	if(cBufferPtr == NULL){ 
		fprintf(stderr, "v: GetImages16(): Couldn't get ByteBuffer memory in JNI land. Is it a direct buffer?");
		fflush(stderr);
		checkReturn("GetImages16", env, -73); //throw PICamSDKException("JNI alloction failed");
		return NULL;
	}
	long bufferLen = env->GetDirectBufferCapacity(jByteBuffer);

	int firstValid = 0, lastValid = 0;
	unsigned int ret = GetImages16(first, last, (unsigned short *)cBufferPtr, bufferLen/2, &firstValid, &lastValid);
	checkReturn("GetOldestImage16", env, ret);

	jintArray retArr = env->NewIntArray(2);
	if (retArr == NULL){
		fprintf(stderr, "Andor2JNI: GetImages16(): Couldn't allocate int array in JNI land.");
		fflush(stderr);
		checkReturn("GetImages16", env, -73); //throw AndorV2Exception("JNI alloction failed");
		return NULL;
	}	

	env->SetIntArrayRegion(retArr, 0, 1, (int32_t*)&firstValid);
	env->SetIntArrayRegion(retArr, 1, 1, (int32_t*)&lastValid);

	return retArr;
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    testCode
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_testCode
  (JNIEnv *env, jclass cls){


	char *lc = setlocale(LC_ALL, NULL);
	printf("locale1 = %s\n", lc);

//	lc = setlocale(LC_ALL, "C");
//	printf("locale2 = %s\n", lc);

	//Initialize takes non-const str, avoid warning
	char pathStr[32];
	strncpy(pathStr, "/usr/local/etc/andor", 32);

	unsigned int error = Initialize(pathStr);
	if(error!=DRV_SUCCESS){
		printf("Initialisation error...exiting");
		return;
	}

	lc = setlocale(LC_ALL, NULL);
	printf("locale3 = %s\n", lc);
	
	float exp, acc, kin;
	GetAcquisitionTimings(&exp, &acc, &kin);
	printf("1: Exposure=%f, Accumulation Cycle=%f, Kinetic Cycle=%f\n", exp, acc, kin);

	
	SetExposureTime(0.1);

	GetAcquisitionTimings(&exp, &acc, &kin);
	printf("2: Exposure=%f, Accumulation Cycle=%f, Kinetic Cycle=%f\n", exp, acc, kin);
	
	ShutDown();
}


/*
 * Class:     andor2JNI_AndorV2
 * Method:    nop
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_nop
  (JNIEnv *env, jclass cls){
printf("fuck off\n");
fflush(stdout);
}

/*
 * Class:     andor2JNI_AndorV2
 * Method:    setBusPortSelection
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_andor2JNI_AndorV2_setBusPortSelection
  (JNIEnv *env, jclass cls, jint bus, jstring jStrPort){

	char str[64];

	if(bus >= 0){
		snprintf(str, sizeof(str), "%d", bus);
		setenv("LIBUSB_SINGLE_BUS", str, -1);
	}else{
		unsetenv("LIBUSB_SINGLE_BUS");
	}

	if(jStrPort != NULL){
		char *cStrPort = (char *)env->GetStringUTFChars(jStrPort, NULL);

		if(cStrPort != NULL && strlen(cStrPort) > 0){
			printf("Set LIBUSB_SINGLE_PORT = %s", cStrPort);
			setenv("LIBUSB_SINGLE_PORT", cStrPort, -1);
		}else{
			unsetenv("LIBUSB_SINGLE_PORT");
		}
		env->ReleaseStringUTFChars(jStrPort, cStrPort);
	}else{
		unsetenv("LIBUSB_SINGLE_PORT");
	}

}


//int main(int argc, char* argv[]){
//	Java_andor2JNI_AndorV2_testCode(NULL, NULL);
//}
