package andor2JNI;

import java.nio.ByteBuffer;

import java.nio.ByteOrder;
import java.nio.IntBuffer;


/** Bitfields for AndorV2.GetCapabilities() */
public class AndorV2Capabilities {

	public static final int AC_ACQMODE_SINGLE = 1;
	public static final int AC_ACQMODE_VIDEO = 2;
	public static final int AC_ACQMODE_ACCUMULATE = 4;
	public static final int AC_ACQMODE_KINETIC = 8;
	public static final int AC_ACQMODE_FRAMETRANSFER = 16;
	public static final int AC_ACQMODE_FASTKINETICS = 32;
	public static final int AC_ACQMODE_OVERLAP = 64;
	
	public static final int AC_READMODE_FULLIMAGE = 1;
	public static final int AC_READMODE_SUBIMAGE = 2;
	public static final int AC_READMODE_SINGLETRACK = 4;
	public static final int AC_READMODE_FVB = 8;
	public static final int AC_READMODE_MULTITRACK = 16;
	public static final int AC_READMODE_RANDOMTRACK = 32;
	public static final int AC_READMODE_MULTITRACKSCAN = 64;
	
	public static final int AC_TRIGGERMODE_INTERNAL = 1;
	public static final int AC_TRIGGERMODE_EXTERNAL = 2;
	public static final int AC_TRIGGERMODE_EXTERNAL_FVB_EM = 4;
	public static final int AC_TRIGGERMODE_CONTINUOUS = 8;
	public static final int AC_TRIGGERMODE_EXTERNALSTART = 16;
	public static final int AC_TRIGGERMODE_EXTERNALEXPOSURE = 32;
	public static final int AC_TRIGGERMODE_INVERTED = 0x40;
	public static final int AC_TRIGGERMODE_EXTERNAL_CHARGESHIFTING = 0x80;
	
	// Deprecated for AC_TRIGGERMODE_EXTERNALEXPOSURE;
	public static final int AC_TRIGGERMODE_BULB = 32;
	
    public static final String cameraTypes[] =  { 
    		"PDA", "IXON", "ICCD", "EMCCD", "CCD", "ISTAR", "VIDEO", "IDUS", "NEWTON", "SURCAM", "USBICCD", "LUCA", "RESERVED", "IKON", "INGAAS", "IVAC", 
    		"UNPROGRAMMED", "CLARA", "USBISTAR", "SIMCAM", "NEO", "IXONULTRA", "VOLMOS", "IVAC_CCD", "ASPEN", "ASCENT", "ALTA", "ALTAF", "IKONXL","RES1", 
    };

	public static final int AC_CAMERATYPE_PDA = 0;
	public static final int AC_CAMERATYPE_IXON = 1;
	public static final int AC_CAMERATYPE_ICCD = 2;
	public static final int AC_CAMERATYPE_EMCCD = 3;
	public static final int AC_CAMERATYPE_CCD = 4;
	public static final int AC_CAMERATYPE_ISTAR = 5;
	public static final int AC_CAMERATYPE_VIDEO = 6;
	public static final int AC_CAMERATYPE_IDUS = 7;
	public static final int AC_CAMERATYPE_NEWTON = 8;
	public static final int AC_CAMERATYPE_SURCAM = 9;
	public static final int AC_CAMERATYPE_USBICCD = 10;
	public static final int AC_CAMERATYPE_LUCA = 11;
	public static final int AC_CAMERATYPE_RESERVED = 12;
	public static final int AC_CAMERATYPE_IKON = 13;
	public static final int AC_CAMERATYPE_INGAAS = 14;
	public static final int AC_CAMERATYPE_IVAC = 15;
	public static final int AC_CAMERATYPE_UNPROGRAMMED = 16;
	public static final int AC_CAMERATYPE_CLARA = 17;
	public static final int AC_CAMERATYPE_USBISTAR = 18;
	public static final int AC_CAMERATYPE_SIMCAM = 19;
	public static final int AC_CAMERATYPE_NEO = 20;
	public static final int AC_CAMERATYPE_IXONULTRA = 21;
	public static final int AC_CAMERATYPE_VOLMOS = 22;
	public static final int AC_CAMERATYPE_IVAC_CCD = 23;
	public static final int AC_CAMERATYPE_ASPEN = 24;
	public static final int AC_CAMERATYPE_ASCENT = 25;
	public static final int AC_CAMERATYPE_ALTA = 26;
	public static final int AC_CAMERATYPE_ALTAF = 27;
	public static final int AC_CAMERATYPE_IKONXL = 28;
	public static final int AC_CAMERATYPE_RES1 = 29;
	
	public static final int AC_PIXELMODE_8BIT = 1;
	public static final int AC_PIXELMODE_14BIT = 2;
	public static final int AC_PIXELMODE_16BIT = 4;
	public static final int AC_PIXELMODE_32BIT = 8;
	
	public static final int AC_PIXELMODE_MONO = 0x000000;
	public static final int AC_PIXELMODE_RGB = 0x010000;
	public static final int AC_PIXELMODE_CMY = 0x020000;
	
	public static final int AC_SETFUNCTION_VREADOUT = 0x01;
	public static final int AC_SETFUNCTION_HREADOUT = 0x02;
	public static final int AC_SETFUNCTION_TEMPERATURE = 0x04;
	public static final int AC_SETFUNCTION_MCPGAIN = 0x08;
	public static final int AC_SETFUNCTION_EMCCDGAIN = 0x10;
	public static final int AC_SETFUNCTION_BASELINECLAMP = 0x20;
	public static final int AC_SETFUNCTION_VSAMPLITUDE = 0x40;
	public static final int AC_SETFUNCTION_HIGHCAPACITY = 0x80;
	public static final int AC_SETFUNCTION_BASELINEOFFSET = 0x0100;
	public static final int AC_SETFUNCTION_PREAMPGAIN = 0x0200;
	public static final int AC_SETFUNCTION_CROPMODE = 0x0400;
	public static final int AC_SETFUNCTION_DMAPARAMETERS = 0x0800;
	public static final int AC_SETFUNCTION_HORIZONTALBIN = 0x1000;
	public static final int AC_SETFUNCTION_MULTITRACKHRANGE = 0x2000;
	public static final int AC_SETFUNCTION_RANDOMTRACKNOGAPS = 0x4000;
	public static final int AC_SETFUNCTION_EMADVANCED = 0x8000;
	public static final int AC_SETFUNCTION_GATEMODE = 0x010000;
	public static final int AC_SETFUNCTION_DDGTIMES = 0x020000;
	public static final int AC_SETFUNCTION_IOC = 0x040000;
	public static final int AC_SETFUNCTION_INTELLIGATE = 0x080000;
	public static final int AC_SETFUNCTION_INSERTION_DELAY = 0x100000;
	public static final int AC_SETFUNCTION_GATESTEP = 0x200000;
	public static final int AC_SETFUNCTION_GATEDELAYSTEP = 0x200000;
	public static final int AC_SETFUNCTION_TRIGGERTERMINATION = 0x400000;
	public static final int AC_SETFUNCTION_EXTENDEDNIR = 0x800000;
	public static final int AC_SETFUNCTION_SPOOLTHREADCOUNT = 0x1000000;
	public static final int AC_SETFUNCTION_REGISTERPACK = 0x2000000;
	public static final int AC_SETFUNCTION_PRESCANS = 0x4000000;
	public static final int AC_SETFUNCTION_GATEWIDTHSTEP = 0x8000000;
	public static final int AC_SETFUNCTION_EXTENDED_CROP_MODE = 0x10000000;
	public static final int AC_SETFUNCTION_SUPERKINETICS = 0x20000000;
	public static final int AC_SETFUNCTION_TIMESCAN = 0x40000000;
	;
	// Deprecated for AC_SETFUNCTION_MCPGAIN;
	public static final int AC_SETFUNCTION_GAIN = 8;
	public static final int AC_SETFUNCTION_ICCDGAIN = 8;
	;
	public static final int AC_GETFUNCTION_TEMPERATURE = 0x01;
	public static final int AC_GETFUNCTION_TARGETTEMPERATURE = 0x02;
	public static final int AC_GETFUNCTION_TEMPERATURERANGE = 0x04;
	public static final int AC_GETFUNCTION_DETECTORSIZE = 0x08;
	public static final int AC_GETFUNCTION_MCPGAIN = 0x10;
	public static final int AC_GETFUNCTION_EMCCDGAIN = 0x20;
	public static final int AC_GETFUNCTION_HVFLAG = 0x40;
	public static final int AC_GETFUNCTION_GATEMODE = 0x80;
	public static final int AC_GETFUNCTION_DDGTIMES = 0x0100;
	public static final int AC_GETFUNCTION_IOC = 0x0200;
	public static final int AC_GETFUNCTION_INTELLIGATE = 0x0400;
	public static final int AC_GETFUNCTION_INSERTION_DELAY = 0x0800;
	public static final int AC_GETFUNCTION_GATESTEP = 0x1000;
	public static final int AC_GETFUNCTION_GATEDELAYSTEP = 0x1000;
	public static final int AC_GETFUNCTION_PHOSPHORSTATUS = 0x2000;
	public static final int AC_GETFUNCTION_MCPGAINTABLE = 0x4000;
	public static final int AC_GETFUNCTION_BASELINECLAMP = 0x8000;
	public static final int AC_GETFUNCTION_GATEWIDTHSTEP = 0x10000;
	;
	// Deprecated for AC_GETFUNCTION_MCPGAIN;
	public static final int AC_GETFUNCTION_GAIN = 0x10;
	public static final int AC_GETFUNCTION_ICCDGAIN = 0x10;
	
	public static final int AC_FEATURES_POLLING = 1;
	public static final int AC_FEATURES_EVENTS = 2;
	public static final int AC_FEATURES_SPOOLING = 4;
	public static final int AC_FEATURES_SHUTTER = 8;
	public static final int AC_FEATURES_SHUTTEREX = 16;
	public static final int AC_FEATURES_EXTERNAL_I2C = 32;
	public static final int AC_FEATURES_SATURATIONEVENT = 64;
	public static final int AC_FEATURES_FANCONTROL = 128;
	public static final int AC_FEATURES_MIDFANCONTROL = 256;
	public static final int AC_FEATURES_TEMPERATUREDURINGACQUISITION = 512;
	public static final int AC_FEATURES_KEEPCLEANCONTROL = 1024;
	public static final int AC_FEATURES_DDGLITE = 0x0800;
	public static final int AC_FEATURES_FTEXTERNALEXPOSURE = 0x1000;
	public static final int AC_FEATURES_KINETICEXTERNALEXPOSURE = 0x2000;
	public static final int AC_FEATURES_DACCONTROL = 0x4000;
	public static final int AC_FEATURES_METADATA = 0x8000;
	public static final int AC_FEATURES_IOCONTROL = 0x10000;
	public static final int AC_FEATURES_PHOTONCOUNTING = 0x20000;
	public static final int AC_FEATURES_COUNTCONVERT = 0x40000;
	public static final int AC_FEATURES_DUALMODE = 0x80000;
	public static final int AC_FEATURES_OPTACQUIRE = 0x100000;
	public static final int AC_FEATURES_REALTIMESPURIOUSNOISEFILTER = 0x200000;
	public static final int AC_FEATURES_POSTPROCESSSPURIOUSNOISEFILTER = 0x400000;
	public static final int AC_FEATURES_DUALPREAMPGAIN = 0x800000;
	public static final int AC_FEATURES_DEFECT_CORRECTION = 0x1000000;
	public static final int AC_FEATURES_STARTOFEXPOSURE_EVENT = 0x2000000;
	public static final int AC_FEATURES_ENDOFEXPOSURE_EVENT = 0x4000000;
	public static final int AC_FEATURES_CAMERALINK = 0x8000000;
	public static final int AC_FEATURES_FIFOFULL_EVENT = 0x10000000;
	public static final int AC_FEATURES_SENSOR_PORT_CONFIGURATION = 0x20000000;
	public static final int AC_FEATURES_SENSOR_COMPENSATION = 0x40000000;
	public static final int AC_FEATURES_IRIG_SUPPORT = 0x80000000;
	
	public static final int AC_EMGAIN_8BIT = 1;
	public static final int AC_EMGAIN_12BIT = 2;
	public static final int AC_EMGAIN_LINEAR12 = 4;
	public static final int AC_EMGAIN_REAL12 = 8;
		
	/**
	typedef struct ANDORCAPS
	{
			0  at_u32 ulSize;
			1  at_u32 ulAcqModes;
			2  at_u32 ulReadModes;
			3  3at_u32 ulTriggerModes;
			4  at_u32 ulCameraType;
			5  at_u32 ulPixelMode;
			6  at_u32 ulSetFunctions;
			7  at_u32 ulGetFunctions;
			8  at_u32 ulFeatures;
			9  at_u32 ulPCICard;
			10 at_u32 ulEMGainCapability;
			11 at_u32 ulFTReadModes;
	} AndorCapabilities*/
	private byte[] structData;
    
	
	public AndorV2Capabilities(byte[] structData) {
		this.structData = structData;
	}

    public byte[] getStructData() { return structData; }
    
    public int getAcqModes(){ return ByteBuffer.wrap(structData).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer().get(1); }
    public int getReadModes(){ return ByteBuffer.wrap(structData).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer().get(2); }
    public int getTriggerModes(){ return ByteBuffer.wrap(structData).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer().get(3); }
    public int getCameraType(){ return ByteBuffer.wrap(structData).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer().get(4); }
    public int getPixelMode(){ return ByteBuffer.wrap(structData).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer().get(5); }
    public int getSetFunctions(){ return ByteBuffer.wrap(structData).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer().get(6); }
    public int getGetFunctions(){ return ByteBuffer.wrap(structData).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer().get(7); }
    public int getFeatures(){ return ByteBuffer.wrap(structData).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer().get(8); }
    public int getPCICard(){ return ByteBuffer.wrap(structData).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer().get(9); }
    public int getEMGainCapability(){ return ByteBuffer.wrap(structData).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer().get(10); }
    public int getFTReadModes(){ return ByteBuffer.wrap(structData).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer().get(11); }

    public int getBitDepth(){
    	switch(getPixelMode()){
	    	case AC_PIXELMODE_8BIT: return 8;
	    	case AC_PIXELMODE_14BIT: return 14;
	    	case AC_PIXELMODE_16BIT: return 16;
	    	case AC_PIXELMODE_32BIT: return 32;
	    	default: throw new IllegalArgumentException("Unrecognised pixel mode " + getPixelMode());
    	}
    }
	   
    public String getCameraTypeName(){
    	int camType = getCameraType();
    	return (camType >= 0 && camType < 30) ? cameraTypes[camType] : ("UnknownCamera_" + camType);
    }
    
    @Override
    public String toString(){
    	IntBuffer iBuff = ByteBuffer.wrap(structData).asIntBuffer();
    	return "AndorV2Capabilities: "
    			+ "ulSize = " + iBuff.get(0)
				+ "ulAcqModes = " + iBuff.get(1)
				+ "ulReadModes = " + iBuff.get(2)
				+ "ulTriggerModes = " + iBuff.get(3)
				+ "ulCameraType = " + iBuff.get(4)
				+ "ulPixelMode = " + iBuff.get(5)
				+ "ulSetFunctions = " + iBuff.get(6)
				+ "ulGetFunctions = " + iBuff.get(7)
				+ "ulFeatures = " + iBuff.get(8)
				+ "ulPCICard = " + iBuff.get(9)
				+ "ulEMGainCapability = " + iBuff.get(10)
				+ "ulFTReadModes = " + iBuff.get(11);
    }
  
}
