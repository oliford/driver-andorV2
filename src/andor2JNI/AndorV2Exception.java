package andor2JNI;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AndorV2Exception extends RuntimeException {

	public static final int ERROR_JNI_ALLOCATION = -73;
	
	public static final int DRV_ERROR_CODES = 20001;
	public static final int DRV_SUCCESS = 20002;
	public static final int DRV_VXDNOTINSTALLED = 20003;
	public static final int DRV_ERROR_SCAN = 20004;
	public static final int DRV_ERROR_CHECK_SUM = 20005;
	public static final int DRV_ERROR_FILELOAD = 20006;
	public static final int DRV_UNKNOWN_FUNCTION = 20007;
	public static final int DRV_ERROR_VXD_INIT = 20008;
	public static final int DRV_ERROR_ADDRESS = 20009;
	public static final int DRV_ERROR_PAGELOCK = 20010;
	public static final int DRV_ERROR_PAGEUNLOCK = 20011;
	public static final int DRV_ERROR_BOARDTEST = 20012;
	public static final int DRV_ERROR_ACK = 20013;
	public static final int DRV_ERROR_UP_FIFO = 20014;
	public static final int DRV_ERROR_PATTERN = 20015;
	
	public static final int DRV_ACQUISITION_ERRORS = 20017;
	public static final int DRV_ACQ_BUFFER = 20018;
	public static final int DRV_ACQ_DOWNFIFO_FULL = 20019;
	public static final int DRV_PROC_UNKONWN_INSTRUCTION = 20020;
	public static final int DRV_ILLEGAL_OP_CODE = 20021;
	public static final int DRV_KINETIC_TIME_NOT_MET = 20022;
	public static final int DRV_ACCUM_TIME_NOT_MET = 20023;
	public static final int DRV_NO_NEW_DATA = 20024;
	public static final int KERN_MEM_ERROR = 20025;
	public static final int DRV_SPOOLERROR = 20026;
	public static final int DRV_SPOOLSETUPERROR = 20027;
	public static final int DRV_FILESIZELIMITERROR = 20028;
	public static final int DRV_ERROR_FILESAVE = 20029;
	
	public static final int DRV_TEMPERATURE_CODES = 20033;
	public static final int DRV_TEMPERATURE_OFF = 20034;
	public static final int DRV_TEMPERATURE_NOT_STABILIZED = 20035;
	public static final int DRV_TEMPERATURE_STABILIZED = 20036;
	public static final int DRV_TEMPERATURE_NOT_REACHED = 20037;
	public static final int DRV_TEMPERATURE_OUT_RANGE = 20038;
	public static final int DRV_TEMPERATURE_NOT_SUPPORTED = 20039;
	public static final int DRV_TEMPERATURE_DRIFT = 20040;
	
	public static final int DRV_TEMP_CODES = 20033;
	public static final int DRV_TEMP_OFF = 20034;
	public static final int DRV_TEMP_NOT_STABILIZED = 20035;
	public static final int DRV_TEMP_STABILIZED = 20036;
	public static final int DRV_TEMP_NOT_REACHED = 20037;
	public static final int DRV_TEMP_OUT_RANGE = 20038;
	public static final int DRV_TEMP_NOT_SUPPORTED = 20039;
	public static final int DRV_TEMP_DRIFT = 20040;
	
	public static final int DRV_GENERAL_ERRORS = 20049;
	public static final int DRV_INVALID_AUX = 20050;
	public static final int DRV_COF_NOTLOADED = 20051;
	public static final int DRV_FPGAPROG = 20052;
	public static final int DRV_FLEXERROR = 20053;
	public static final int DRV_GPIBERROR = 20054;
	public static final int DRV_EEPROMVERSIONERROR = 20055;
	
	public static final int DRV_DATATYPE = 20064;
	public static final int DRV_DRIVER_ERRORS = 20065;
	public static final int DRV_P1INVALID = 20066;
	public static final int DRV_P2INVALID = 20067;
	public static final int DRV_P3INVALID = 20068;
	public static final int DRV_P4INVALID = 20069;
	public static final int DRV_INIERROR = 20070;
	public static final int DRV_COFERROR = 20071;
	public static final int DRV_ACQUIRING = 20072;
	public static final int DRV_IDLE = 20073;
	public static final int DRV_TEMPCYCLE = 20074;
	public static final int DRV_NOT_INITIALIZED = 20075;
	public static final int DRV_P5INVALID = 20076;
	public static final int DRV_P6INVALID = 20077;
	public static final int DRV_INVALID_MODE = 20078;
	public static final int DRV_INVALID_FILTER = 20079;
	
	public static final int DRV_I2CERRORS = 20080;
	public static final int DRV_I2CDEVNOTFOUND = 20081;
	public static final int DRV_I2CTIMEOUT = 20082;
	public static final int DRV_P7INVALID = 20083;
	public static final int DRV_P8INVALID = 20084;
	public static final int DRV_P9INVALID = 20085;
	public static final int DRV_P10INVALID = 20086;
	public static final int DRV_P11INVALID = 20087;
	
	public static final int DRV_USBERROR = 20089;
	public static final int DRV_IOCERROR = 20090;
	public static final int DRV_VRMVERSIONERROR = 20091;
	public static final int DRV_GATESTEPERROR = 20092;
	public static final int DRV_USB_INTERRUPT_ENDPOINT_ERROR = 20093;
	public static final int DRV_RANDOM_TRACK_ERROR = 20094;
	public static final int DRV_INVALID_TRIGGER_MODE = 20095;
	public static final int DRV_LOAD_FIRMWARE_ERROR = 20096;
	public static final int DRV_DIVIDE_BY_ZERO_ERROR = 20097;
	public static final int DRV_INVALID_RINGEXPOSURES = 20098;
	public static final int DRV_BINNING_ERROR = 20099;
	public static final int DRV_INVALID_AMPLIFIER = 20100;
	public static final int DRV_INVALID_COUNTCONVERT_MODE = 20101;
	
	public static final int DRV_ERROR_NOCAMERA = 20990;
	public static final int DRV_NOT_SUPPORTED = 20991;
	public static final int DRV_NOT_AVAILABLE = 20992;
	
	public static final int DRV_ERROR_MAP = 20115;
	public static final int DRV_ERROR_UNMAP = 20116;
	public static final int DRV_ERROR_MDL = 20117;
	public static final int DRV_ERROR_UNMDL = 20118;
	public static final int DRV_ERROR_BUFFSIZE = 20119;
	public static final int DRV_ERROR_NOHANDLE = 20121;
	
	public static final int DRV_GATING_NOT_AVAILABLE = 20130;
	public static final int DRV_FPGA_VOLTAGE_ERROR = 20131;
	
	public static final int DRV_OW_CMD_FAIL = 20150;
	public static final int DRV_OWMEMORY_BAD_ADDR = 20151;
	public static final int DRV_OWCMD_NOT_AVAILABLE = 20152;
	public static final int DRV_OW_NO_SLAVES = 20153;
	public static final int DRV_OW_NOT_INITIALIZED = 20154;
	public static final int DRV_OW_ERROR_SLAVE_NUM = 20155;
	public static final int DRV_MSTIMINGS_ERROR = 20156;
	
	public static final int DRV_OA_NULL_ERROR = 20173;
	public static final int DRV_OA_PARSE_DTD_ERROR = 20174;
	public static final int DRV_OA_DTD_VALIDATE_ERROR = 20175;
	public static final int DRV_OA_FILE_ACCESS_ERROR = 20176;
	public static final int DRV_OA_FILE_DOES_NOT_EXIST = 20177;
	public static final int DRV_OA_XML_INVALID_OR_NOT_FOUND_ERROR = 20178;
	public static final int DRV_OA_PRESET_FILE_NOT_LOADED = 20179;
	public static final int DRV_OA_USER_FILE_NOT_LOADED = 20180;
	public static final int DRV_OA_PRESET_AND_USER_FILE_NOT_LOADED = 20181;
	public static final int DRV_OA_INVALID_FILE = 20182;
	public static final int DRV_OA_FILE_HAS_BEEN_MODIFIED = 20183;
	public static final int DRV_OA_BUFFER_FULL = 20184;
	public static final int DRV_OA_INVALID_STRING_LENGTH = 20185;
	public static final int DRV_OA_INVALID_CHARS_IN_NAME = 20186;
	public static final int DRV_OA_INVALID_NAMING = 20187;
	public static final int DRV_OA_GET_CAMERA_ERROR = 20188;
	public static final int DRV_OA_MODE_ALREADY_EXISTS = 20189;
	public static final int DRV_OA_STRINGS_NOT_EQUAL = 20190;
	public static final int DRV_OA_NO_USER_DATA = 20191;
	public static final int DRV_OA_VALUE_NOT_SUPPORTED = 20192;
	public static final int DRV_OA_MODE_DOES_NOT_EXIST = 20193;
	public static final int DRV_OA_CAMERA_NOT_SUPPORTED = 20194;
	public static final int DRV_OA_FAILED_TO_GET_MODE = 20195;
	
	public static final int DRV_PROCESSING_FAILED = 20211;
	
	
	private String functionName;
	private int errorCode;
	
	private static final HashMap<Integer, String> errorStrings = new HashMap<Integer, String>();
	
	
	static {
		Field[] declaredFields = AndorV2Exception.class.getDeclaredFields();		
		for (Field field : declaredFields) {
		    if (java.lang.reflect.Modifier.isStatic(field.getModifiers()) &&
		    		field.getName().startsWith("DRV_")) {
		    	
		    	try {
					errorStrings.put(field.getInt(null), field.getName());
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
		    }
		}
		
		errorStrings.put(ERROR_JNI_ALLOCATION, "JNI alloction failed");
	}

	public static final String getErrorString(int returnCode){
			//convert to string here if possible
			String str = errorStrings.get(returnCode);
						
			return (str != null) ? str : ("Unknown Andor V2 SDK error '" + String.format("%d", returnCode) + "'.");
	}
	
	public AndorV2Exception(int errorCode) {
		this(getErrorString(errorCode));	
		
		this.functionName = "?";
		this.errorCode = errorCode;		
	}
	
	public AndorV2Exception(String functionName, int errorCode) {
		this(functionName + " returned the error "+String.format("%x", errorCode)+": " + getErrorString(errorCode));	
		
		this.functionName = functionName;
		this.errorCode = errorCode;		
	}

	public AndorV2Exception(String errorString) { 
		super(errorString);
		this.errorCode = -1;
	}

	public int getErrorCode() { return errorCode; }

	public String getErrorString(){ return getErrorString(errorCode); }			

}
