import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Locale;

import andor2JNI.AndorV2;
import andor2JNI.AndorV2Defs;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

/* Andor example program showing the use of the SDK to
  perform a Run Till Abort acquisition from
  the CCD.*/

public class RTA {

	public static void main(String[] args) throws IOException, InterruptedException {		
		new SettingsManager("minerva", true);
		
        //String cmd = "/opt/andor-2.102-30045/examples/console/rta/rta";
		/*String cmd = "/home/specadmin/minerva/code/driver-andorV2/jni/linkTest";
        
        // Get runtime
        java.lang.Runtime rt = java.lang.Runtime.getRuntime();
        // Start a new process: UNIX command ls
        java.lang.Process p = rt.exec(cmd);
        // You can or maybe should wait for the process to complete
        p.waitFor();
        System.out.println("Process exited with code = ");// + rt.exitValue());
        
        // Get process' output: its InputStream
        java.io.InputStream is = p.getInputStream();
        java.io.BufferedReader reader = new java.io.BufferedReader(new InputStreamReader(is));
        // And print each line
        String s = null;
        while ((s = reader.readLine()) != null) {
            System.out.println(s);
        }
        is.close();
        
        //*/
		
		
		
		//AndorV2.testCode();
		
		//AndorV2.testCode();
		//if(true)return;

		if (CameraSelect(args) < 0) {
			System.err.println("*** CAMERA SELECTION ERROR");
			System.exit(-1);
		}

		long error;
		boolean quit;
		char choice;
		float fChoice;
		int width, height;
		long lNumberInSeries = 10;

		System.out.print("Initializing library... ");
		//Initialize CCD
		AndorV2.Initialize("/usr/local/etc/andor");
		System.out.print("OK.");
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) { }
		
		{
			float fRetA[] = AndorV2.GetAcquisitionTimings();
			float expA = fRetA[0], accA = fRetA[1], kinA = fRetA[2];
			System.out.println("Init Timings: Exposure=" + expA + ", Accumulation Cycle=" + accA + ", Kinetic Cycle=" + kinA);
		}
		
		try {
			//sleep to allow initialization to complete
			Thread.sleep(2);
		} catch (InterruptedException e) { } 

		//Set Read Mode to --Image--
		AndorV2.SetReadMode(4);

		
		//Set Acquisition mode to --Single scan--
		AndorV2.SetAcquisitionMode(5);

		//Set initial exposure time
		AndorV2.SetExposureTime((float)0.1);
		
		{
			float fRetA[] = AndorV2.GetAcquisitionTimings();
			float expA = fRetA[0], accA = fRetA[1], kinA = fRetA[2];
			System.out.println("Init2 Timings: Exposure=" + expA + ", Accumulation Cycle=" + accA + ", Kinetic Cycle=" + kinA);
		}
		

		//Get Detector dimensions
		int iRet[] = AndorV2.GetDetector();
		width = iRet[0]; height = iRet[1];

		//Initialize Shutter to always open
		AndorV2.SetShutter(1,1,0,0);

		//Setup Image dimensions
		AndorV2.SetImage(1,1,1,width,1,height);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		quit = false;
		do{
			//Show menu options
			System.out.println("        Menu\n"
					+ "================================\n" +
					"a. Start Acquisition\n" +
					"b. Set Exposure Time\n" +
					"n. Set Number Images to acquire\n" +
					"z.     Exit\n" +
					"================================\n" +
					"Choice?::");

			
			
			//Get menu choice
			try {
				String line = null;
				while(line == null || line.length() == 0)
					line = br.readLine();
				choice = line.charAt(0);
			} catch (IOException err) {
				throw new RuntimeException(err);
			}

			switch(choice){
			case 'a': //Acquire
			{
				AndorV2.StartAcquisition();

				int status;

				ByteBuffer imageData = ByteBuffer.allocateDirect(width*height*2);
				imageData.order(ByteOrder.LITTLE_ENDIAN);

				int lAcquired = 0;
				int count = 0;
				while(lAcquired<lNumberInSeries){
					//Loop until acquisition finished
					AndorV2.WaitForAcquisition();

					lAcquired = AndorV2.GetTotalNumberImagesAcquired();
					AndorV2.GetMostRecentImage16(imageData);

					System.out.print("Image " + lAcquired + " Data[0-9]={");
					for(int i=0; i<10; i++){
						System.out.print(imageData.getShort(i*2) + ",");
					}
					System.out.println("}");
					count++;
				}

				AndorV2.AbortAcquisition();

				//delete imageData;
			}

			break;

			case 'b': //Set new exposure time

				System.out.println("Enter new Exposure Time(s)::");				
				fChoice = (float) Algorithms.mustParseDouble(br.readLine());

				AndorV2.SetExposureTime(fChoice);

				float fRet[] = AndorV2.GetAcquisitionTimings();
				float exp = fRet[0], acc = fRet[1], kin = fRet[2];

				System.out.println("New Timings: Exposure=" + exp + ", Accumulation Cycle=" + acc + ", Kinetic Cycle=" + kin);
				break;

			case 'n': //Number in series

				System.out.println("Enter Number in Series::");
				lNumberInSeries = Algorithms.mustParseInt(br.readLine());

				break;

			case 'z': //Exit

				quit = true;

				break;

			default:

				System.out.println("!Invalid Option!");

			} 

			try {
				System.in.read();
			} catch (IOException e) { }

		}while(!quit);	

		//Shut down CCD
		AndorV2.ShutDown();	

		System.exit(0);
	}

	private static int CameraSelect(String args[])
	{
		if (args.length == 2) {

			int lNumCameras = AndorV2.GetAvailableCameras();
			int iSelectedCamera = Integer.parseInt(args[1]);

			if (iSelectedCamera < lNumCameras && iSelectedCamera >= 0) {
				int lCameraHandle = AndorV2.GetCameraHandle(iSelectedCamera);
				AndorV2.SetCurrentCamera(lCameraHandle);
				return iSelectedCamera;
			}
			else
				return -1;
		}
		return 0;
	}
}