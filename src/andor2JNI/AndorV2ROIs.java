package andor2JNI;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;

public class AndorV2ROIs {
    
	public static class AndorV2ROI {
		//non-PI things useful in ImageProc
		public String name;
		public boolean enabled;
		
    	public int x;
    	public int width;
    	public int x_binning;
    	public int y;
    	public int height;
    	public int y_binning;
    	
    	@Override
    	public String toString() {
    		return "{"+name+","
    				+ (enabled?"Enabled,":"Disabled,")
    				+ x + "," + width + "," + x_binning + ","
    				+ y + "," + height + "," + y_binning + "}";
    	}
    }
    
    private ArrayList<AndorV2ROI> rois;
    
    public AndorV2ROIs(){
    	rois = new ArrayList<AndorV2ROI>();
    }
    
    public AndorV2ROIs(byte structData[]){
    	int nROIs = structData.length / (6*4);
    	
    	ByteBuffer buff = ByteBuffer.wrap(structData);
    	buff.order(ByteOrder.LITTLE_ENDIAN);
    	
    	rois = new ArrayList<AndorV2ROI>(nROIs);
    	
    	for(int i=0; i < nROIs; i++){
    		AndorV2ROI roi = new AndorV2ROI();
    		roi.name = "Loaded_"+i;
    		roi.enabled = true;
    		roi.x = buff.getInt();
    		roi.width = buff.getInt();
    		roi.x_binning = buff.getInt();
    		roi.y = buff.getInt();
    		roi.height = buff.getInt();
    		roi.y_binning = buff.getInt();
    		rois.add(roi);
    	}
    }
    
    /**  (I'm not sure this is useful since the library can't return the current configuration anyway)
     	Each track must be defined by a group of six integers.
		-The top and bottom positions of the tracks.
		-The left and right positions for the area of interest within each track
		-The horizontal and vertical binning for each track.
		 See also AndorV2.SetComplexImage()
     */
    public void updateFromAreasArray(int areas[]){
    	int nROIs = areas.length / 6;
    	
    	rois = new ArrayList<AndorV2ROI>(nROIs);
    	
    	for(int i=0; i < nROIs; i++){
    		AndorV2ROI roi = new AndorV2ROI();
    		roi.name = "Auto_"+i;
    		roi.enabled = true;
    		
    		roi.x = areas[i*6 + 0];
    		roi.width = areas[i*6 + 1] - areas[i*6 + 0] + 1;
    		roi.y = areas[i*6 + 2];
    		roi.height = areas[i*6 + 3] - areas[i*6 + 2] + 1;
    		
    		roi.x_binning = areas[i*6 + 4];
    		roi.y_binning = areas[i*6 + 5];
    		rois.add(roi);
    	}
    }
    
    /** Each track must be defined by a group of six integers.
		-The top and bottom positions of the tracks.
		-The left and right positions for the area of interest within each track
		-The horizontal and vertical binning for each track.
		 See also AndorV2.SetComplexImage()
     */
    public int[] toAreasArray(){
    	synchronized (rois) {
    		int nEnabled = 0;
    		for(AndorV2ROI roi : rois)
    			if(roi.enabled)
    				nEnabled++;
    		
			int areasArray[] = new int[nEnabled * 6];
	
			int j=0;
	    	for(AndorV2ROI roi : rois){	    
	    		if(!roi.enabled)
	    			continue;
	    		
	    		areasArray[j++] = roi.y + 1;
	    		areasArray[j++] = roi.y + roi.height;
	    		areasArray[j++] = roi.x + 1;
	    		areasArray[j++] = roi.x + roi.width;
	    		areasArray[j++] = roi.x_binning;
	    		areasArray[j++] = roi.y_binning;
	    	}
	    	
	    	return areasArray;
    	}
    }

    public void addROI(int x, int width, int x_binning, int y, int height, int y_binning){
    	AndorV2ROI roi = new AndorV2ROI();
    	roi.x = x;
    	roi.width = width;
    	roi.x_binning = x_binning;
    	roi.y = y;
    	roi.height = height;
    	roi.y_binning = y_binning;
    	rois.add(roi);
    }

	public void addROI(AndorV2ROI roi) {
		synchronized (rois) {
			rois.add(roi);
    	}
	} 
	
	public AndorV2ROI getROI(String name){
		for(AndorV2ROI roi : rois)
			if(name.equals(roi.name))
				return roi;
		return null;
	}

	@Override
	public String toString() {
		StringBuffer strB = new StringBuffer();
		strB.append("ROIs[");
		synchronized (rois) {
			for(AndorV2ROI roi : rois)
				strB.append(roi);
		}
		strB.append("]");
		return strB.toString();
	}

	public void remove(AndorV2ROI roi) {
		rois.remove(roi);
	}
	
	public ArrayList<AndorV2ROI> getROIListCopy() {
		synchronized (rois) {
			return (ArrayList<AndorV2ROI>)rois.clone();
		}
	}

	public void addArraysToMap(HashMap<String, Object> map, String prefix, boolean enabledOnly) {
		synchronized (rois) {
				
			ArrayList<String> roisNames = new ArrayList<>();
			ArrayList<Integer> roisX = new ArrayList<>();
			ArrayList<Integer> roisWidth = new ArrayList<>();
			ArrayList<Integer> roisXBinning = new ArrayList<>();
			ArrayList<Integer> roisY = new ArrayList<>();
			ArrayList<Integer> roisHeight = new ArrayList<>();
			ArrayList<Integer> roisYBinning = new ArrayList<>();
			ArrayList<Integer> roisEnabled = new ArrayList<>();
			
			int idx = 0;
			for(AndorV2ROI roi : rois){
				if(enabledOnly && !roi.enabled)
					continue;
				
				roisNames.add(roi.name);
				roisX.add(roi.x);
				roisWidth.add(roi.width);
				roisXBinning.add(roi.x_binning);
				roisY.add(roi.y);
				roisHeight.add(roi.height);
				roisYBinning.add(roi.y_binning);
				roisEnabled.add(roi.enabled ? 1 : 0);
				
				idx++;
			}
			
			map.put(prefix + "/names", roisNames.toArray(new String[idx]));
			map.put(prefix + "/x", roisX.toArray(new Integer[idx]));
			map.put(prefix + "/width", roisWidth.toArray(new Integer[idx]));
			map.put(prefix + "/x_binning", roisXBinning.toArray(new Integer[idx]));
			map.put(prefix + "/y", roisY.toArray(new Integer[idx]));
			map.put(prefix + "/height", roisHeight.toArray(new Integer[idx]));
			map.put(prefix + "/y_binning", roisYBinning.toArray(new Integer[idx]));			
			map.put(prefix + "/enabled", roisEnabled.toArray(new Integer[idx]));
		}
	}
}
