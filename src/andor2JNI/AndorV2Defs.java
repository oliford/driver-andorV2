package andor2JNI;

public class AndorV2Defs {
	
	public static final String acqusitionModes[] = { "Invalid", "Single Scan", "Accumulate", "Kinetics", "Fast Kinetics", "Run Till Abort" };

	public static final int ACQUSITIONMODE_SINGLE_SCAN = 1;
	public static final int ACQUSITIONMODE_ACCUMULATE = 2;
	public static final int ACQUSITIONMODE_KINETICS = 3;
	public static final int ACQUSITIONMODE_FAST_KINETICS = 4;
	public static final int ACQUSITIONMODE_RUN_TILL_ABORT = 5;
	
	public static final String readModes[] = { "Full vertical binning", "Multi track", "Random track", "Single track", "Image" };
	
	public static final int READMODE_FULL_VERTICAL_BINNING = 0;	
	public static final int READMODE_MULTI_TRACK = 1;	
	public static final int READMODE_RANDOM_TRACK = 2;	
	public static final int READMODE_SINGLE_TRACK = 3;	
	public static final int READMODE_IMAGE = 4;
	
	public static final String shutterTypes[] = { "TTL Low=Open", "TTL High=Open" };
	public static final int SHUTTERTYPE_OPEN_LOW = 0;
	public static final int SHUTTERTYPE_OPEN_HIGH = 1;
	
	public static final String shutterModes[] = { "Auto", "Open", "Closed", "Invalid", "Open for FVB", "Open for series" };

	public static final int SHUTTERMODE_FULLY_AUTO = 0;
	public static final int SHUTTERMODE_OPEN = 1;
	public static final int SHUTTERMODE_CLOSED = 2;
	public static final int SHUTTERMODE_INVALID = 3;
	public static final int SHUTTERMODE_OPEN_FVB = 4;
	public static final int SHUTTERMODE_OPEN_SERIES = 5;

	public static final String amplifiers[] = { "EMCCD", "Conventional" };

	public static final int OUTPUTAMPLIFIER_EMCCD = 0;
	public static final int OUTPUTAMPLIFIER_CONVENTIONAL = 1;
	
	public static final String triggerModes[] = { "Internal", "External", "External Start", "?=3", "?=4", "?=5", "?=6", 
			          "External Exposure", "?=8", "External FVB EM", "Software", "External charge shift" };
				
	public static final int TRIGGERMODE_INTERNAL = 0;
	public static final int TRIGGERMODE_EXTERNAL = 1;
	public static final int TRIGGERMODE_EXTERNALSTART = 2;
	public static final int TRIGGERMODE_EXTERNALEXPOSURE = 7;	
	public static final int TRIGGERMODE_EXTERNAL_FVB_EM = 9;
	public static final int TRIGGERMODE_SOFTWARE = 10;
	public static final int TRIGGERMODE_EXTERNAL_CHARGESHIFTING = 11;

	/** copied from AndorV2Exception, but not really errors */

	public static final int DRV_TEMP_CODES = 20033;
	public static final int DRV_TEMP_OFF = 20034;
	public static final int DRV_TEMP_NOT_STABILIZED = 20035;
	public static final int DRV_TEMP_STABILIZED = 20036;
	public static final int DRV_TEMP_NOT_REACHED = 20037;
	public static final int DRV_TEMP_OUT_RANGE = 20038;
	public static final int DRV_TEMP_NOT_SUPPORTED = 20039;
	public static final int DRV_TEMP_DRIFT = 20040;
	
	public static String tempCodeToString(int tempCode){
		switch(tempCode){
			case DRV_TEMP_OFF: return "Off"; 
			case DRV_TEMP_NOT_STABILIZED: return "Not Stabilised"; 
			case DRV_TEMP_STABILIZED: return "Stabilised";
			case DRV_TEMP_NOT_REACHED: return "Not reached"; 
			case DRV_TEMP_OUT_RANGE: return "Out of range";
			case DRV_TEMP_NOT_SUPPORTED: return "Not supported"; 
			case DRV_TEMP_DRIFT: return "Drift";
			default: return "Unrecognied ("+tempCode+")";
		}
	}
	
	//also AC_xxx in Caps
	public static final int EMGAIN_8BIT = 1;
	public static final int EMGAIN_12BIT = 2;
	public static final int EMGAIN_LINEAR12 = 4;
	public static final int EMCCD_GAIN_REAL12 = 8;
	
	public static final String[] emccdGainModes = { "Invalid", "8-Bit", "12-Bit", "Linear", "Real" };
	
}
